<?php
  session_start();
  require_once('navbar.php');
?>

<!doctype html>
<html lang="fr">
<head>
  <title>Accueil de Mini-Pinterest</title>

</head>
<body>


  <h1 style="margin-left: 5px">Toutes les photos</h1>
  <div>
	  <?php
      if (isset($_SESSION['pseudo'])) {
        $pseudo = $_SESSION['pseudo'];
      }
      else {
        $pseudo = "";
      }
      $tabNomImages = getNomImages($link);
      echo "<div class= 'd-flex flex-wrap justify-content-around'>";
      foreach ($tabNomImages as $nomFich) {
        if(!estCachee($link, $nomFich) || isAdministrateur($link, $pseudo)) {
          echo "<div style = 'margin : auto' class = 'text-alogn center'  >";
          echo "<form action='description.php' method='post' >";
          echo "<input type='hidden' name='id[]' value='$nomFich'>";
          echo "<input type='image'  name='soumettre' value=''  src='assets/images/$nomFich' class = 'rounded mx-auto d-block' onclick='submit();' >";
          echo "</form>";
          echo "</div>";
        }
        
      
      }
      echo "</div>";
    
	  ?>
  </div>

	

</body>
</html>
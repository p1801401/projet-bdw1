<?php
  session_start();
  require_once("fonctions/bd.php");
	require_once("fonctions/utilisateur.php");
  require_once("fonctions/image.php");
  $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
	if (isset($_POST['deconnexion'])){
    setDisconnected($link, $_SESSION['pseudo']);
    unset($_SESSION['pseudo']);
  }
  require_once("navbar.php");
?>

<!doctype html>
<html lang="fr">
<head>
<title>Connexion</title>
</head>
<body>

	<?php
		if (isset($_POST['connexion']))
    {
      $query = "SELECT pseudo, mdp FROM utilisateur";
      $res = executeQuery($link, $query);
      while ($row = $res->fetch_assoc())
      {
          if ( ($row['pseudo'] == $_POST['pseudo']) && ($row['mdp'] == md5($_POST['mdp'])) )
          {
              $_SESSION["pseudo"] = $_POST['pseudo'];
              setConnected($link, $_POST['pseudo']);
              $_SESSION["temps"] = time();
              header ('Location: menu.php');
          }
      }
      echo "<div class='alert alert-warning' role='alert'>
              Le couple pseudo/mot de passe ne correspond à aucun utilisateur enregistré
            </div>";
    }
	?>


<form action = "index.php" method="post">
  <div class="form-group">
    <label for="identifiant">Identifiant</label>
    <input id="identifiant" type="text" class="form-control" name = 'pseudo' >
  </div>
  <div class="form-group">
    <label for="motdepasse">Mot de passe</label>
    <input id="motdepasse" type="password" class="form-control" name ="mdp">
  </div>

  <button type="submit" name = "connexion" class="btn btn-primary">Se connecter</button>
</form>
	<a href='inscription.php'>Première connexion?</a>
	

</body>
</html>
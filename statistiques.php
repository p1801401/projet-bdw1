<?php
    session_start();
    require_once("navbar.php");
?>

<!doctype html>
<html lang="fr">
<head>

<title>Description de la photo</title>

</head>

<body>

<div class= 'd-flex flex-wrap justify-content-around' style ='margin-top:8%'>

    <form action="pageAdmin.php" method="post">
        <div class="form-col align-items-center">

            <h2>Supprimer une image</h2>
            <form action ="suppression.php" method="post" >
                
            <div class="form-group">
                <label for="image">Fichier de l'image</label>
                <input id="image" type="text" class="form-control" name='image' >
                <button class="btn btn-outline-danger" type="submit" name="suppImage">Supprimer</button>
                
            </div>

        </div>
    </form>


    <form action="pageAdmin.php" metho="post">
        <div class="form-col align-items-center">
            <h2>Supprimer un utilisateur</h2>
            <div class="form-group">
                <label for="user">Pseudo de l'utilisateur</label>
                <input id="user" type="text" class="form-control" name = 'pseudoUser' >
                <button class="btn btn-outline-danger" type="submit" name="suppUser">Supprimer</button>
            </div>
        </div>
    </form>

    <form action="pageAdmin.php" method="post">
        <div class="form-col align-items-center">
            <h2>Supprimer une catégorie</h2>
            <div class="form-group">
                    <label for="cat">Nom de la catégorie</label>
                    <input id="cat" type="text" class="form-control" name ='categorie_a_supprimer' >
                    <button class="btn btn-outline-primary " type="submit" name="suppCat">Supprimer</button>
            </div>
        </div>
    </form> 

    <form action="pageAdmin.php" method="post">
        <div class="form-col align-items-center">
            <h2>Devenir administrateur</h2>
            <div class="form-group">
                    <label for="userA">Pseudo de l'utilisateur</label>
                    <input id="userA" type="text" class="form-control" name = 'userToA' >
                    <button class="btn btn-outline-primary " type="submit" name="devenir">Devenir</button>
            </div>
        </div>
    </form>



</div>
    
    
    
    
<div  style ='margin-top:8%'>
    
    
    <div class= 'text-align-center' style ='margin-top:8%'>    
        <?php
        
            
            $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
            $utilisateurs = getListUsers($link);
            $categories = getListCategories($link);
            echo "<h1> Voici les statistiques du site </h1>";
            echo "<table class='table'>
            <thead class='thead-dark'>
                <tr>
                <th scope='col'>Id</th>
                <th scope='col'>Utilisateurs</th>
                <th scope='col'>Photos</th>
                <th scope='col'>Etat</th>
                </tr>
            </thead>
            <tbody>";
            $nbConnecte = 0;
            $nbImages = 0;
            $nbImagesTotal = 0;
            $ImagesDeUtilisateur; 
            foreach ($utilisateurs as $auteur){
                $ImagesDeUtilisateur = getImagesUtilisateur($link,$auteur);
                $nbImages = count($ImagesDeUtilisateur, COUNT_NORMAL);
                echo   
                "<tr>
                <th scope='row'>".getIdFromPseudo($link,$auteur)."</th>
                <td>$auteur</td>
                <td>$nbImages</td>";

                if(isConnected($link, $auteur)){
                    echo "<td>Connecte</td>";
                    $nbConnecte = $nbConnecte + 1;
                }
                else {
                    echo "<td>Deconnecte</td>";                       
                }
                echo "</tr>";

                $nbImagesTotal = $nbImagesTotal + $nbImages;
            
            }

            echo "<th scope='row'> Total  </th>
            <td>".count($utilisateurs, COUNT_NORMAL)."</td>
            <td>$nbImagesTotal</td>
            <td>$nbConnecte</td>";



            echo "
            <thead class='thead-dark'>
                <tr>
                <th scope='col'>Id</th>
                <th scope='col'>Categorie</th>
                <th scope='col'>Photos</th>
                <th scope='col'>Fichier Photo</th>
                </tr>
            </thead>";
            $nbImages = 0;

            $testVide;

            foreach ($categories as $cat){

                $testVide = getImagesFromCategorie($link, $cat);

                if(!empty($testVide)){
                
                    $nbImages = count(getImagesFromCategorie($link, $cat), COUNT_NORMAL);
                    echo   
                    "<tr>
                    <th scope='row'>".getIdFromCategorie($link,$cat)."</th>
                    <td>$cat</td>
                    
                    <td>$nbImages</td>

                    <td>";

                    foreach(getImagesFromCategorie($link, $cat) as $imageCat){
                        echo "<p>$imageCat</p> <br>";
                    }

                    echo "</td>";
                    echo "</tr>";
                }
                        
            }
        ?>
        </table>
    </div>

</div>


</body>
</html>
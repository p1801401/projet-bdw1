<?php
  session_start();
  require_once("navbar.php");
?>

<!doctype html>
<html lang="fr">




<head>
  <meta charset="utf-8">
  <title>Premi&egrave;re inscription</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</head>
<body>
  <?php
    if (isset($_POST["inscrire"]))
    {
      $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
      if ($_POST['pseudo'] != NULL && $_POST['password'] != NULL){
        if (strcmp($_POST['password'], $_POST['confirmer_password']) == 0) {
          if (checkAvailability($_POST['pseudo'], $link) == true){
            register ($_POST['pseudo'], md5($_POST['password']), $link);
            header ('Location: index.php');
          }
          else {
            echo "<div class='alert alert-warning' role='alert'>
              Pseudo déjà existant.
            </div>";
          }
        }
        else {
          echo "<div class='alert alert-warning' role='alert'>
              Veuillez entrer le même mot de passe.
            </div>";
        }  
      }
      else {
        echo "<div class='alert alert-warning' role='alert'>
              Veuillez tout remplir.
            </div>";
      }
    }
  ?>

<style type="text/css">
      body {
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }

      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
      .form-signin {
		text-align: center;  
	  }
	  p{
		text-align: center;    
	  }
	  img{
		    display: block;
			margin: auto;
			padding: 20px;

	  }
      h1{
		 font-size: 36px; 
		 text-align: center;
		 font-weight: bold;
	  }
    
    </style>

  <div style="margin-top: 150px">
  <h1 class="form-signin-heading">Inscription</h1>
  <div class="container">
    	<form class="form-signin" action="inscription.php" method="post">
        <input id="pseudo" type="text" class="input-block-level" name="pseudo" placeholder="Pseudo" />
        <input id="password" type="password" class="input-block-level" name="password" placeholder="Mot de passe" />
        <input id="confirmer_password" type="password" class="input-block-level" name="confirmer_password" placeholder="Confirmer mot de passe" />
        <button class="btn btn-large btn-primary" type="submit" name="inscrire">S'inscrire</button>
      </form>
      <p><a href='index.php'>Déjà inscrit?</a></p>
  </div>
  </div>    

</body>
</html>
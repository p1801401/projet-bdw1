<?php
	require_once("fonctions/bd.php");
  require_once("fonctions/utilisateur.php");
  require_once("fonctions/image.php");
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="js/chessboard-1.0.0.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</head>
<body>

  <div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">Mini Pinterest</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="menu.php">Accueil <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Catégories
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <form action="categorie.php" method="get">

              <?php
                $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
                $tabNomCat = getNomCategories($link);
                foreach ($tabNomCat as $nomCategorie) {
                  echo "<input class='dropdown-item' type='submit' name='categorie' value = '$nomCategorie'>";
                }
              
              ?>
              
              </form>
            </div>
          </li>
        </ul>

        <?php

          if (isset($_SESSION['pseudo'])){
          

            if (isConnected($link, $_SESSION["pseudo"])) {

              if(isAdministrateur($link, $_SESSION["pseudo"])){
                echo "<li class='nav-item active form-inline my-2 my-lg-0' style='margin-right: 10px' >
                <form action='statistiques.php'>
                  <button class='btn btn-outline-dark' name='statistiques' type ='submit'> Statistiques </button>
                </form>
                </li>";            
                
              }

              echo "<li class='nav-item active form-inline my-2 my-lg-0' style='margin-right: 10px' >
              <form action='ajoutImage.php' method='post'>
                <button class='btn btn-outline-dark' name='ajoutImage' type ='submit'> Ajouter une Image </button>
              </form>
              </li>";

              echo "<li class='nav-item active form-inline my-2 my-lg-0' style='margin-right: 10px'>
              <form action= 'profil.php' method ='post'>
                <button class='btn btn-outline-primary my-2 my-sm-0' type ='submit' name='profil'>Mon Profil</button>
              </form>
              </li>";           

 
            }

            echo "<form class='form-inline my-2 my-lg-0' action='index.php' method ='post'>
              <button class='btn btn-outline-danger my-2 my-sm-0' type='submit' name ='deconnexion'>Deconnexion</button>
            </form>";


            $tmp = time() -  $_SESSION["temps"];

            echo "<li class='nav-item active  my-2 my-lg-0' style='margin-left: 10px' >
            <span> durée de connection : ".$tmp." sec
            </li>";

            echo "<li class='nav-item active  my-2 my-lg-0' style='margin-left: 10px' >
            <span> ". $_SESSION['pseudo'] ."
            </li>";

            echo "<svg class='bi bi-people-circle' style='margin-left: 20px' width='3em' height='3em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
            <path d='M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 008 15a6.987 6.987 0 005.468-2.63z'/>
            <path fill-rule='evenodd' d='M8 9a3 3 0 100-6 3 3 0 000 6z' clip-rule='evenodd'/>
            <path fill-rule='evenodd' d='M8 1a7 7 0 100 14A7 7 0 008 1zM0 8a8 8 0 1116 0A8 8 0 010 8z' clip-rule='evenodd'/>
          </svg>";
      

          
          }

          else {
            echo "<form class='form-inline my-2 my-lg-0' action='index.php' method='post'>
              <button class='btn btn-outline-success my-2 my-sm-0' type='submit'>Connexion</button>
            </form>"; 
          
          }


          ?>

      </div>
    </nav>
  </div>

 
</body>
</html>
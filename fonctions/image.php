<?php

    require_once("utilisateur.php");

    //fonction permettant d'avoir la liste des noms des images presentent dans la base de données
    
    function getNomImages($link) {
        $query = "SELECT nomFich FROM photo";
        $res = executeQuery($link, $query);
        $array = array();
        while ($row = $res->fetch_assoc()) {
             $array[] = $row['nomFich'];
        }
        return $array;
    }

    //fonction permettant d'avoir la liste des categories presentent dans la base de données
    function getNomCategories($link) {
        $query = "SELECT nomCat FROM categorie";
        $res = executeQuery($link, $query);
        $array = array();
        while ($row = $res->fetch_assoc()) {
            $array[] = $row['nomCat'];
        }
        return $array;
    }

    // fonction permettant d'avoir la liste des noms des images dans une categorie mise en parametre
    function getImagesFromCategorie($link, $categorie) {
        $query = "SELECT p.nomFich FROM photo p join categorie c on p.catId = c.catId where c.nomCat = '$categorie' ";
        $res = executeQuery($link, $query);
        while ($row = $res->fetch_assoc()) {
            $array[] = $row['nomFich'];
        }
        if(!empty($array)){
            return $array; 
        }
               
    }

    // fonction permettant d'avoir la liste des noms des images ajouté par un utilisateur mis en parametre
    function getImagesUtilisateur($link, $pseudo)
    {
        $userId = getIdFromPseudo($link,$pseudo);
        $query = "SELECT p.nomFich FROM photo p JOIN utilisateur u ON p.auteurId = u.userId WHERE p.auteurId = '$userId'";
        $res = executeQuery($link, $query);
        $array = array();
        while ($row = $res->fetch_assoc()) {
            $array[] = $row['nomFich'];
        }
        return $array; 

    }
    
    // fonction permettant d'obtenir la description d'une image donc le nom fichier est mis en parametre
    function getDescriptionFromImage($link, $nomFichier) {
        $query = "SELECT description FROM photo WHERE nomFich = '$nomFichier'";
        $res = executeQuery($link, $query);
        $row = $res->fetch_assoc();
        return $row['description'];
    }

    // fonction permettant de recupererla categorie auquel appartient une image dont le nom fichier est mis en parametre
    function getCategorieFromImage($link, $nomFichier) {
        $query = "SELECT c.nomCat FROM categorie c WHERE c.catId IN (SELECT p.catId FROM photo p WHERE p.nomFich = '$nomFichier')";
        $res = executeQuery($link, $query);
        $row = $res->fetch_assoc();
        return $row['nomCat'];
    }

    //fonction permettant d'avoir la liste des categories presentent dans la base de données
    function getListCategories ($link) {

        $query = "SELECT nomCat FROM categorie";
        $res = executeQuery($link, $query);
        $array = array();
        while ($row = $res->fetch_assoc()) {
            if(!empty($row['nomCat'])){
                $array[] = $row['nomCat'];
            }
        }
        return $array; 
    }

    //fonction permettant d'avoir le catId a partir du nom de sa categorie
    function  getIdFromCategorie($link,$categorie){

        $query = "SELECT catId FROM categorie WHERE nomCat = '$categorie'";
        $res = executeQuery($link, $query);
        $row = $res->fetch_assoc();
        return $row['catId'];
    
    }


    //fonction permettant d'avoir le photoId a partir du nom de la photo
    function getIdFromImage($link, $nomFich){

        $query = "SELECT photoId FROM photo WHERE nomFich = '$nomFich'";
        $res = executeQuery($link, $query);
        $row = $res->fetch_assoc();
        return $row['photoId'];

    }

    //fonction permettant d'ajouter une photo dans la base de données. Elle verifie aussi si la photo n'est pas déjà présente dans la base de données.
    function addImage($link, $nomFich, $description, $IdCat, $auteurId) {
        
        $verif = "SELECT photoId FROM photo WHERE nomFich = '$nomFich'";
        $res = executeQuery($link, $verif);
        $row = $res->fetch_assoc();
        if (empty($row['photoId'])) {
            $query = "INSERT INTO photo (nomFich, description, catId, auteurId)  
                VALUES ('$nomFich', '$description', '$IdCat', '$auteurId')";
            executeUpdate($link, $query);
        }

    }

    //fonction permettant de changer le nom d'un fichier image
    function changeNomFich($link, $newName, $oldName){

        $query = "UPDATE photo SET nomFich = '$newName' WHERE nomFich = '$oldName' ";
        executeUpdate($link, $query);
    }

    //fonction permettant d'avoir la liste des noms des images presentent dans la base de données
    function getListImages($link){
        
        $query = "SELECT nomFich FROM photo";
        $res = executeQuery($link, $query);
        $array = array();
        while ($row = $res->fetch_assoc()) {
            $array[] = $row['nomFich'];
        }
        return $array; 
    }

    //fonction permettant de supprimer une image dans la base de données
    function removeImage($link, $nomFich){
        $query ="DELETE FROM photo WHERE nomFich= '$nomFich' ";
        executeUpdate($link, $query);
    }

    //fonction permettant d'ajouter une categorie dans la base de données
    function addCategorie($link, $categorie){

        $nbCategorie = count(getListCategories($link), COUNT_NORMAL) + 1;

        $verif = "SELECT catId FROM categorie WHERE nomCat = '$categorie' ";
        $res = executeQuery($link, $verif);
        $row = $res->fetch_assoc();
        if (empty($row['catId'])) {
            $query = "INSERT INTO categorie (catId, nomCat) VALUES ($nbCategorie,'$categorie')";
            executeUpdate($link, $query);
        }
    }

    //fonction permettant de recuperer le pseudo de l'auteur d'une image
    function getAuteurFromImage($link, $nomFich) {
        $query = "SELECT u.pseudo 
                  FROM utilisateur u JOIN photo p ON u.userId = p.auteurId 
                  WHERE p.nomFich = '$nomFich'";
        $res = executeQuery($link, $query);
        $row = $res->fetch_assoc();
        return $row['pseudo'];
    }

    //fonction permettant de savoir si une image est cachée ou non
    function estCachee($link, $nomFich) {
        $query = "SELECT hidden FROM photo WHERE nomFich = '$nomFich'";
        $res = executeQuery($link, $query);
        $row = $res->fetch_assoc();
        return $row['hidden'] != 0;
    }



    //fonction permettant de suppriemr une categorie dans la base de données. 
    //Il faut que cette categorie soit vide (c'est-a-dire qu'il n'y a aucune image dans cette categorie) pour que celle-ci soit supprimée
    function removeCategorie($link,$categorie){

        $verif = "SELECT nomCat FROM categorie WHERE nomCat = '$categorie' ";
        $res = executeQuery($link, $verif);
        $row = $res->fetch_assoc();
        if (!empty($row['nomCat'])) {
            $query = "DELETE FROM categorie WHERE nomCat = '$categorie' ";
            executeUpdate($link, $query);
        }
    }


?>

<?php

$dbHost = "localhost";
$dbUser = "root";
$dbPwd = "";
$dbName = "tp_mini_pinterest";


// fonction permettant de se connecter à la base de données
function getConnection($dbHost, $dbUser, $dbPwd, $dbName)
{
	$mysqli = new mysqli($dbHost, $dbUser, $dbPwd, $dbName);
	if ($mysqli->connect_errno) {
    echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
		//echo $mysqli->host_info . "\n";

		$mysqli = new mysqli("127.0.0.1", $dbUser, $dbPwd, $dbName, 3306);
		if ($mysqli->connect_errno) {
		echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}



return $mysqli;
}


//fonction permettant d'executer une requette. $query doit être écrit en SQL
function executeQuery($link, $query)
{
	$res = $link->query($query);
	if ($res == false)
	{
		echo "Echec de la requête  " . $link->error ;
	}
	return $res;
}


//fonction permettant d'executer une requette. $query doit être écrit en SQL
function executeUpdate($link, $query)
{
	$res = $link->query($query);
	if ($res == false)
	{
		echo "Echec de la requête  " . $link->error ;
	}
}


//fonction permettant de se deconnecter de la base de données
function closeConnexion($link)
{
	$link->close();
}
?>
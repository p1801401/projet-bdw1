<?php
    require_once("image.php");

//fonction permettant de savoir si un pseudo a déjà été pris, si elle est déjà dans de la base de données 
function checkAvailability($pseudo, $link)
{
    $query = "SELECT pseudo FROM utilisateur WHERE pseudo = '$pseudo'";
    $res = executeQuery($link, $query);
    $row = $res->fetch_assoc();
    if ($row == NULL) {
        return true;
    }
    return false;
}

//fonction permettant d'ajouter un nouvel utilisateur dans la base de données
function register($pseudo, $hashPwd, $link)
{
    $query = "INSERT INTO utilisateur (pseudo, mdp, etat, type) 
              VALUES ('$pseudo', '$hashPwd', 'deconnecte', 'utilisateur')";
	executeUpdate($link, $query);
}

//fonction permettant changer l'etat d'un utilisateur et de le mettre l'etat "connecte"
function setConnected($link, $pseudo)
{
    $query = "UPDATE utilisateur SET etat = 'connecte' WHERE pseudo = '$pseudo'";
    executeUpdate($link, $query);
}

//fonction permettant de savoir si l'utilisateur a entrer le bon couple de pseudo/mot de passe
function getUser($pseudo, $hashPwd, $link)
{
    $query = "SELECT pseudo, mdp FROM utilisateur";
    $res = executeQuery($link, $query);
    while ($row = $res->fetch_assoc()) {
        if (($row['pseudo'] == $pseudo) && ($row['mdp'] == $hashPwd)) {
            return true;
        }
    }
    return false;
}

//fonction permettant de recupérer le pseudo des utilisateurs connectés
function getConnectedUsers($link)
{
    $query = "SELECT pseudo FROM utilisateur WHERE etat = 'connected'";
    $res = executeQuery($link, $query);
    $array = array();
    while ($row = $res->fetch_assoc()) {
        $array[] = $row['pseudo'];
    }
    return $array;
}

//fonction permettant de changer l'etat d'un utilisateur et de le mettre à l'etat "deconnecte"
function setDisconnected($link, $pseudo)
{
	$query = "UPDATE utilisateur SET etat = 'deconnecte' WHERE pseudo = '$pseudo'";
    executeUpdate($link, $query);
}

/*function setType($pseudo, $link, $type)
{
    $query = "UPDATE utilisateur SET type = '$type' WHERE pseudo = '$pseudo' ";
    executeUpdate($link, $query);
}*/

//fonction permettant de verifier si un utilisateur est conneccté
function isConnected($link, $pseudo)
{
    $query = "SELECT etat FROM utilisateur WHERE pseudo = '$pseudo'" ;
    $res = executeQuery($link, $query);
    while ($row = $res->fetch_assoc()) {
        if ($row['etat'] == "connecte")  {
            return true;
        }
    }
    return false;
}

//fonction permettant de verifier si un utilisateur est un administrateur
function isAdministrateur($link, $pseudo)
{
    $query = "SELECT type FROM utilisateur WHERE pseudo = '$pseudo'" ;
    $res = executeQuery($link, $query);
    while ($row = $res->fetch_assoc()) {
        if ($row['type'] == "administrateur")  {
            return true;
        }
    }
    return false;
}

//fonction permettant de recupérer la liste des pseudo des administrateurs
function getAdministrateurs($link)
{
    $query = "SELECT pseudo FROM utilisateur WHERE type = 'administrateur'";
    $res = executeQuery($link, $query);
    $array = array();
    while ($row = $res->fetch_assoc()) {
        $array[] = $row['pseudo'];
    }
    return $array;   
}


//fonction permettant de changer le type d'un utilisateur et de le mettre en "administrateur"
function setAdministrateur($link, $pseudo){
    $query ="UPDATE utilisateur SET type = 'administrateur' WHERE pseudo = '$pseudo'";
    executeUpdate($link, $query);
}


//fonction permettant de récupérer un tableau contenant les pseudo de tout les utilisateur
function getListUsers($link)
{
    $query = "SELECT pseudo FROM utilisateur";
    $res = executeQuery($link, $query);
    $array = array();
    while ($row = $res->fetch_assoc()) {
        $array[] = $row['pseudo'];
    }
    return $array; 
}

//fonction permettant de récupérer l'id d'un utilisateur a partir de son pseudo
function  getIdFromPseudo($link,$pseudo){

    $query = "SELECT userId FROM utilisateur WHERE pseudo = '$pseudo'";
    $res = executeQuery($link, $query);
    $row = $res->fetch_assoc();
    return $row['userId'];

}

//fonction permettant de supprimer un utilisateur de la base de données
function removeUser($link, $pseudo){

    $query = "DELETE FROM utilisateur WHERE pseudo= '$pseudo' "; 
    executeUpdate($link, $query);
}


?>
<?php
	session_start();
	require_once("navbar.php");
?>

<!doctype html>
<html lang="fr">
<head>
<title>Mon Profil</title>
</head>

<body>

	<?php

	echo "<h1 style='text-align: center'>Mon Profil</h1>";
	if(isset($_POST['confirmer_pseudo'])) {
		$link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
		if(checkAvailability($_POST['nouveau_pseudo'], $link)) {
			$ancien_pseudo = $_SESSION['pseudo'];
			$nouveau_pseudo = $_POST['nouveau_pseudo'];
			$query = "UPDATE utilisateur SET pseudo = '$nouveau_pseudo' WHERE pseudo ='$ancien_pseudo'";
			executeUpdate($link, $query);
			$_SESSION['pseudo'] = $nouveau_pseudo;
			echo "<div class='alert alert-success' role='alert'>
			Votre pseudo a bien été modifié!
		  	</div>";
		}
		else {
			echo "<div class='alert alert-warning' role='alert'>
			Le pseudo choisi existe déjà.
		  	</div>";
		}
		
	}
	if(isset($_POST['confirmer_mdp'])) {
		$link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
		$pseudo = $_SESSION['pseudo'];
		$query = "SELECT mdp FROM utilisateur WHERE pseudo = '$pseudo'";
		$res = executeQuery($link, $query);
		$row = $res->fetch_assoc();
		if ($row['mdp'] == md5($_POST['ancien_mdp']) && $_POST['nouveau_mdp'] == $_POST['confirm_nouveau_mdp']){
			$nouveau_mdp = md5($_POST['nouveau_mdp']);
			$query = "UPDATE utilisateur SET mdp = '$nouveau_mdp' WHERE pseudo ='$pseudo'";
			executeUpdate($link, $query);
			echo "<div class='alert alert-success' role='alert'>
			Votre mot de passe a bien été modifié!
		  	</div>";
		}
		else {
			echo "<div class='alert alert-danger' role='alert'>
			Erreur lors de la modification. Vérifiez bien les informations saisies.
		  	</div>";
		}

		
	}
	if(isset($_POST['profil'])){
		echo "<h5 style='text-align: center; margin-top: 40px'>Connecté en tant que " . $_SESSION['pseudo'] . "</h5>";
		echo "	<form style='text-align: center' action='profil.php' method='post'>
					<button class='btn btn-outline-dark btn-lg' style='margin-bottom: 5px' type='submit' name='changer_pseudo'>Changer pseudo</button><br>
					<button class='btn btn-outline-dark btn-lg' type='submit' name='changer_mdp'>Changer mot de passe</button>
				</form>";
		$link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
		$monPseudo = $_SESSION['pseudo'];
		$tabMesPhotos = getImagesUtilisateur($link, $monPseudo);
		echo "<h3 style='text-align: center; margin-top: 40px'>Mes Photos:<h3>";
		echo "<div class= 'd-flex flex-wrap justify-content-around'>";
		if(!empty($tabMesPhotos)){

			foreach ($tabMesPhotos as $nomPhoto) {
				
				echo "<div style = 'margin : auto' class = 'text-alogn center'  >";
				echo "<form action='description.php' method='post' >";
				echo "<input type='hidden' name='id[]' value='$nomPhoto'>";
				echo "<input type='image'  name='soumettre' value=''  src='assets/images/$nomPhoto' class = 'rounded mx-auto d-block' onclick='submit();' >";
				echo "</form>";
				echo "</div>";
			}
			
		}
		else {
			echo "<h5 style = 'color: red'> Vous n'avez aucune photo :( </h5>";
		}

		echo "</div>";
		
	}
	if(isset($_POST['changer_pseudo'])){
		
		echo "<div class= 'd-flex flex-wrap justify-content-around' style ='margin-top:8%'>";
		echo "<div class='form-col align-items-center'>";
		echo "<h3 style='text-align: center; margin-top: 40px'>Modifier mon pseudo</h5>";
		echo "	<form style='text-align: center' action='profil.php' method='post'>
				<div class='form-group'>
					<input class='form-control'  type='text' name='nouveau_pseudo' placeholder='Nouveau pseudo'></input><br>
					<button class='btn btn-outline-success btn-lg' type='submit' name='confirmer_pseudo'>Confirmer</button>
					<button class='btn btn-outline-danger btn-lg' type='submit' name='profil'>Annuler</button>
				</div>
				</form>
				</div>
				</div>";
	}
	if(isset($_POST['changer_mdp'])){
		echo "<div class= 'd-flex flex-wrap justify-content-around' style ='margin-top:8%'>";
		echo "<div class='form-col align-items-center'>";
		echo "<h3 style='text-align: center; margin-top: 40px; margin-bottom: 25px'>Modifier mon mot de passe</h5>";
		echo "	<form style='text-align: center' action='profil.php' method='post'>
				<div class='form-group'>
					<input type='password' name='ancien_mdp' placeholder='Mot de passe actuel'></input><br>
				</div>
				<div class='form-group'>
					<input type='password' name='nouveau_mdp' placeholder='Nouveau mot de passe'></input><br>
				</div>
				<div class='form-group'>
					<input type='password' name='confirm_nouveau_mdp' placeholder='Confirmer mot de passe'></input><br>
				</div>
					<button class='btn btn-outline-success btn-lg' type='submit' name='confirmer_mdp'>Confirmer</button>
					<button class='btn btn-outline-danger btn-lg' type='submit' name='profil'>Annuler</button>
				</form>
				</div>
				</div>";
	}
	
	

	?> 




</body>
</html>
<?php
    session_start();
    require_once("navbar.php");
?>

<!doctype html>
<html lang="fr">
<head>

<title>Ajouter une image</title>

</head>

<body>

    <div class= 'd-flex flex-wrap justify-content-around' style ='margin-top:5%'>
        
        <div class="form-col align-items-center">

            <h1>Ajouter une image</h1>

            <div style= "margin-top: 30px">

                <form action ="importImage.php" method="post" enctype="multipart/form-data" >
                    
                    <div class="col-auto my-1" >
                    <select class="custom-select mr-sm-2" name="categorie" id="inlineFormCustomSelect">

                    <?php
                
                        $taille = count(getListCategories($link), COUNT_NORMAL);

                        echo "<option> Choisissez une catégorie </option>";

                        foreach(getListCategories($link) as $cat){

                            echo "<option>". $cat. "</option>";

                        }
                        echo "</select>
                        </div>";

                    ?>

                    <div class="form-group" style="margin-top: 20px" >
                        <input class="form-control" type="text"  name="nouvelleCategorie" placeholder ="Créer votre catégorie">
                    </div>

                    <div class="form-group" >
                        <textarea class="form-control"  name="description" placeholder = "Décrivez votre image"></textarea>
                    </div>

                    <div class="input-group">
                        <input type='hidden' name='id[]' value='$nomFich'>
                        <input type="file" class="custom-file-input" name="nomFich" id="inputGroupFile04" aria-describedby="inputGroupFileAddon04">
                        <label class="custom-file-label" for="inputGroupFile04">Choisissez une Image</label>
                    </div> 
                    <div class="d-flex flex-wrap justify-content-around" style="margin-top: 50px">
                        <button class="btn btn-outline-dark btn-lg btn-block" type="submit" name="ajoutImage">Ajouter</button>
                    </div>


                </form>
            </div>
        </div>

    </div>


</body>
</html>
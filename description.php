<?php
    session_start();
    require_once("navbar.php");
?>

<!doctype html>
<html lang="fr">
<head>

<title>Description de la photo</title>

</head>

<body>
    
        
        <?php
            if(isset($_POST['confirmer_desc'])) {
                $nomFichier = $_SESSION['nomFich'];
                $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
                $nouvDesc = $_POST['nouv_desc'];
                $query = "UPDATE photo SET description= '$nouvDesc' WHERE nomFich = '$nomFichier'";
                executeUpdate($link, $query);
                echo "<div class='alert alert-success' role='alert'>
			            La description a bien été modifiée!
		  	          </div>";
            }
            
            if(isset($_POST['confirmer_cat'])) {
                $nomFichier = $_SESSION['nomFich'];
                $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
                $nouvCat = $_POST['categorie'];
                $nouvCatId = getIdFromCategorie($link, $nouvCat);
                $query = "UPDATE photo SET catId= '$nouvCatId' WHERE nomFich = '$nomFichier'";
                executeUpdate($link, $query);
                echo "<div class='alert alert-success' role='alert'>
			            La catégorie a bien été modifiée!
		  	          </div>";
            }

            if(isset($_POST['visible'])) {
                $nomFichier = $_SESSION['nomFich'];
                $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
                $query = "UPDATE photo SET hidden = 0 WHERE nomFich = '$nomFichier'";
                executeUpdate($link, $query);
                echo "<div class='alert alert-success' role='alert'>
			            L'image est maintenant visible pour tout le monde!
		  	          </div>";
            }

            if(isset($_POST['invisible'])) {
                $nomFichier = $_SESSION['nomFich'];
                $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
                $query = "UPDATE photo SET hidden = 1 WHERE nomFich = '$nomFichier'";
                executeUpdate($link, $query);
                echo "<div class='alert alert-success' role='alert'>
			            L'image est maintenant invisible pour tout le monde!
		  	          </div>";
            }
            

            if(isset($_POST['id'][0])) {
                if (isset($_SESSION['pseudo'])) {
                    $pseudo = $_SESSION['pseudo'];
                  }
                  else {
                    $pseudo = "";
                  }
                $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
                $nomFichier = $_POST['id'][0];
                $_SESSION['nomFich'] = $nomFichier;
                $description = getDescriptionFromImage($link, $nomFichier);
                $categorie = getCategorieFromImage($link, $nomFichier);
                if (estCachee($link, $nomFichier)) {
                    echo "<div class='alert alert-warning' role='alert'>
			                L'image est actuellement invisible pour les autres utilisateurs.
		  	              </div>";
                } 
                echo "<div class= 'd-flex flex-wrap justify-content-around' style ='margin-top:8%'>";
		        echo "<div class='form-col align-items-center'>";
                echo "<h2> Détails de la photo </h2>";
                echo "<div class='float-left'>";
                echo "<img src='assets/images/$nomFichier' class='rounded'>";
                echo "</div>";
                echo "<div class='float-left'>";
                echo "<table class='table'>
                        <tbody>
                        <tr>
                            <th scope='row'>Description</th>
                            <td>$description</td>
                        </tr>                       
                        <tr>
                            <th scope='row'>Nom du fichier</th>
                            <td>$nomFichier</td>
                        </tr>
                        <tr>
                            <th scope='row'>Catégorie</th>
                            <td><a href='categorie.php?categorie=" . "$categorie" . "'>$categorie</a></td>
                        </tr>
                        <tr><td></td><td></td></tr>
                        </tbody>
                       
                    </table>";
                
                $auteur = getAuteurFromImage($link, $nomFichier);
                if ($auteur == $pseudo || isAdministrateur($link, $pseudo)) {
                    echo "	<form style='margin-left: 5px' action='description.php' method='post'>
                                <button class='btn btn-outline-dark btn-sm' style='margin-bottom: 5px' type='submit' name='modifier_desc'>Modifier description</button><br>
                                <button class='btn btn-outline-dark btn-sm' style='margin-bottom: 5px' type='submit' name='modifier_cat'>Modifier catégorie</button><br>";
                                if (estCachee($link, $nomFichier)) {
                                    echo "<button class='btn btn-outline-dark btn-sm' style='margin-bottom: 5px' type='submit' name='visible'>Rendre visible</button><br>";
                                }
                                else {
                                    echo "<button class='btn btn-outline-dark btn-sm' style='margin-bottom: 5px' type='submit' name='invisible'>Rendre invisible</button><br>";
                                }
                                
                            echo "<button class='btn btn-outline-danger btn-sm' type='submit' name='supprimer'>Supprimer</button>
                            </form>";
                }
                echo "</div>
                        </div>
                        </div>";
                
            }

            if(isset($_POST['modifier_desc'])) {
                
                $nomFichier = $_SESSION['nomFich'];
                echo "<h1 style='text-align: center; margin-top: 40px'>Modification de la catégorie</h1>";
                echo "<div class= 'd-flex flex-wrap justify-content-around' style ='margin-top:8%'>";
		        echo "<div class='form-col align-items-center'>";
                echo "<h3 style='text-align: center; margin-top: 40px'>Choisissez votre nouvelle description</h3>";
                echo "  <form style='text-align: center' action='description.php' method='post'>
                            <textarea style='margin-bottom: 5px' name='nouv_desc' rows='5' cols='50' placeholder='Entrer votre nouvelle description'></textarea><br>
                            <button class='btn btn-outline-success' type='submit' name='confirmer_desc'>Confirmer</button>
                            <button class='btn btn-outline-danger' type='submit' name='annuler_desc'>Annuler</button>
                            <input type='hidden' name='id[]' value='$nomFichier'>
                        </form>
                        </div>
                        </div>";
            }

            if(isset($_POST['modifier_cat'])) {
                $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
                $nomFichier = $_SESSION['nomFich'];
                echo "<h1 style='text-align: center; margin-top: 40px'>Modification de la catégorie</h1>";
                echo "<div class= 'd-flex flex-wrap justify-content-around' style ='margin-top:8%'>";
                echo "<div class='form-col align-items-center'>";
                echo "<h5 style='text-align: center; margin-top: 40px'>Veuillez choisir une catégorie</h5>";
                echo "<form style='text-align: center' action='description.php' method='post'>";
                echo "<div class='col-auto my-1' >
                        <select class='custom-select mr-sm-2' name='categorie' id='inlineFormCustomSelect'>";
                            $taille = count(getListCategories($link), COUNT_NORMAL);
                            
                            foreach(getListCategories($link) as $cat){
    
                                echo "<option>". $cat. "</option>";
    
                            }
    
                            echo "</select>
                            </div>";
                echo "  
                            <button class='btn btn-outline-success' type='submit' name='confirmer_cat'>Confirmer</button>
                            <button class='btn btn-outline-danger' type='submit' name='annuler_cat'>Annuler</button>
                            <input type='hidden' name='id[]' value='$nomFichier'>
                        </form>";
                echo "</div>";
                echo "</div>";
            }

            if(isset($_POST['supprimer'])) {
                $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
                $nomFichier = $_SESSION['nomFich'];
                $query = "DELETE FROM photo WHERE nomFich = '$nomFichier'";
                executeUpdate($link, $query);
                echo "<div class='alert alert-success' role='alert'>
			            Photo supprimée avec succès ! Vous pouvez retourner à votre page de profil.
		  	          </div>";
            }
            
        ?>


</body>
</html>
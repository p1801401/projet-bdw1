<?php
	session_start();
	require_once("navbar.php");
?>

<!doctype html>
<html lang="fr">
<head>
<title>Categorie</title>
</head>

<body>


	<?php



	if(isset($_GET['categorie'])){

		if (isset($_SESSION['pseudo'])) {
			$pseudo = $_SESSION['pseudo'];
		}
		else {
			$pseudo = "";
		}
		$link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
		$categorie = $_GET['categorie'];
		$tabNomFich = getImagesFromCategorie($link, $categorie);
		

		echo "<h2> Voici les images de la categorie : " . $categorie . "</h2>";

		echo "<div class= 'd-flex flex-wrap justify-content-around'>";

		if(!empty($tabNomFich)){

			foreach ($tabNomFich as $nomFich) {
				if (!estCachee($link, $nomFich) || isAdministrateur($link, $pseudo)) {
					echo "<div style = 'margin : auto' class = 'text-alogn center'  >";
					echo "<form action='description.php' method='post' >";
					echo "<input type='hidden' name='id[]' value='$nomFich'>";
					echo "<input type='image'  name='soumettre' value=''  src='assets/images/$nomFich' class = 'rounded mx-auto d-block' onclick='submit();' >";
					echo "</form>";
					echo "</div>";
				}
				
			}
			
		}
		else{
			echo "<h2> Il n'y aucune image dans cette catégorie pour l'instant </h2>";
		}

		echo "</div>";
	}

	?> 



</body>
</html>
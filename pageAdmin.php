<?php
    require_once("fonctions/bd.php");
    require_once("fonctions/utilisateur.php");
    require_once("fonctions/image.php");
    require_once("statistiques.php");
?>


<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Suppression</title>

</head>

<body>

    <?php

        $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);

        if(isset($_POST['suppImage'])){
            removeImage($link, $_POST['image']);
            if (file_exists ("assets/images".$_POST['image'])){

                unlink("assets/images".$_POST['image']);

            }
            
        }


        if(isset($_POST['suppUser'])){
            removeUser($link, $_POST['pseudoUser']);
        }



        if(isset($_POST['devenir'])){
            setAdministrateur($link, $_POST['userToA']);
        }

        if(isset($_POST['suppCat'])){         

            removeCategorie($link, $_POST['categorie_a_supprimer']);
        }

    ?>




</body>
</html>
<?php
    require_once("fonctions/bd.php");
    require_once("fonctions/utilisateur.php");
    require_once("fonctions/image.php");
    require_once("ajoutImage.php");
?>


<!doctype html>
<html lang="fr">

<head>
<meta charset="utf-8">
<title>Ajouter une image</title>

</head>

<body>



    <?php

        if(isset($_POST['ajoutImage'])){

            $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
            $auteurId=getIdFromPseudo($link,$_SESSION['pseudo']);

            
            if(isset($_POST['description'])){
                $description = $_POST['description'];
                if(empty($description) || $description == " "){
                    echo "<div class='alert alert-warning' role='alert'>
                    Veuillez décrire votre image.
                    </div>";
                }
                
            } 

            if(isset($_POST['nouvelleCategorie']) || isset($_POST['categorie'])){


                if($_POST['nouvelleCategorie'] != NULL && $_POST['categorie'] == "Choisissez une catégorie"){
                   
                    addCategorie($link, $_POST['nouvelleCategorie']);
                    $IdCat = getIdFromCategorie($link, $_POST['nouvelleCategorie']);
                
                }

                else if($_POST['categorie'] == "Choisissez une catégorie"){
                    echo "<div class='alert alert-warning' role='alert'>
                    Veuillez choisir une catégorie. 
                    </div>";

                }
                else if(isset($_POST['categorie']) && $_POST['nouvelleCategorie'] == ""){
                    $IdCat = getIdFromCategorie($link, $_POST['categorie']); 

                }
                
            }

            if(isset($_FILES['nomFich'])){

                if ($_FILES['nomFich']['size'] <= 100000)
                {
                    $infosfichier = pathinfo($_FILES['nomFich']['name']);
                    $extension_upload = $infosfichier['extension'];
                    $extensions_autorisees = array('jpeg', 'gif', 'png');

                    if (in_array($extension_upload, $extensions_autorisees))
                    {
                        move_uploaded_file($_FILES['nomFich']['tmp_name'], 'assets/images/'.$_FILES['nomFich']['name']);
                    }
                    else {
                        echo "<div class='alert alert-warning' role='alert'>
                        Ce format d'image n'est pas acceptée.  
                        </div>";
                    }
                }
                else{
                    echo "<div class='alert alert-warning' role='alert'>
                    Votre image est trop lourd (votre fichier doit faire moins de 100ko) !  
                    </div>";
                }

                $images = getNomImages($link);
                foreach ($images as $img ){

                    if($img == $_FILES['nomFich']){
                        
                        echo "<div class='alert alert-warning' role='alert'>
                        Malheureusement quelqu'un a déjà ajouté cette photo
                        </div>";
                    }
                }
                
                addImage($link, $_FILES['nomFich']['name'], $description, $IdCat, $auteurId);

                $photoId = getIdFromImage($link, $_FILES['nomFich']['name']);
                $nomFich = "DSC_".$photoId.".".basename($_FILES['nomFich']['type']);
                
                changeNomFich($link, $nomFich,$_FILES['nomFich']['name']);

                rename("assets/images/".$_FILES['nomFich']['name'] , "assets/images/$nomFich");
            
            }

        }

    ?>




</body>
</html>